import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

import java.math.BigDecimal;

@CsvRecord(separator = ",", crlf = "WINDOWS")
public class Item {

    @DataField(pos = 1)
    private String UniqueID;

    @DataField(pos = 2)
    private String ProductCode;

    @DataField(pos = 3)
    private String ProductName;

    @DataField(pos = 4, precision = 2)
    private BigDecimal priceWholesale;

    @DataField(pos = 5, precision = 2)
    private BigDecimal priceRetail;

    @DataField(pos = 6)
    private Long inStock;


    public String getUniqueID() {
        return UniqueID;
    }

    public void setUniqueID(String uniqueID) {
        UniqueID = uniqueID;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public BigDecimal getPriceWholesale() {
        return priceWholesale;
    }

    public void setPriceWholesale(BigDecimal priceWholesale) {
        this.priceWholesale = priceWholesale;
    }

    public BigDecimal getPriceRetail() {
        return priceRetail;
    }

    public void setPriceRetail(BigDecimal priceRetail) {
        this.priceRetail = priceRetail;
    }

    public Long getInStock() {
        return inStock;
    }

    public void setInStock(Long inStock) {
        this.inStock = inStock;
    }
}
