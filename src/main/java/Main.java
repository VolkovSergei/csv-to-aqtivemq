import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spi.DataFormat;

import java.util.concurrent.atomic.AtomicLong;

public class Main {
    public static void main(String[] args) throws Exception {

        CamelContext context = new DefaultCamelContext();

        ActiveMQConnectionFactory cf = new ActiveMQConnectionFactory();
        cf.setBrokerURL("tcp://localhost:61616");
        cf.setUserName("admin");
        cf.setPassword("admin");

        JmsComponent jmsComponent = new JmsComponent();
        jmsComponent.setConnectionFactory(cf);
        context.addComponent("jms", jmsComponent);

        final DataFormat bindy = new BindyCsvDataFormat(Item.class);

        // add our route to the CamelContext
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("file:data/inbox?noop=true&recursive=true&include=.*csv$")
                        .log("Processing file: ${header.CamelFileName}")
                        .split(body().tokenize("\n")).streaming().parallelProcessing()
                        .log("== line: ${body}")
                        .unmarshal(bindy)
                        .marshal().json(JsonLibrary.Jackson)
                        .log("== JSON: ${body}")
                        .to("jms:csv.record")
                        .process(new Processor() {
                            private final AtomicLong count = new AtomicLong(1);
                            @Override
                            public void process(Exchange exchange) throws Exception {
                                log.info("Processed lines and sent to ActiveMQ: " + count.getAndIncrement());
                            }
                        })
                        .end();
            }
        });

        while (true){
            context.start();
        }
//        context.start();
//        Thread.sleep(10000);
//        context.stop();

    }
}
